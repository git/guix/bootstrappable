title: Bootstrapping JVM languages
---

## Clojure

The Clojure language is bootstrapped on GNU Guix.

Clojure just did the right thing regarding bootstrappability. They have a
bootstrappable build system (Ant), and they have their core in a bootstrappable
language (Java), that is just enough to build the parts of the Clojure complier
so that it can build its Clojure files.

## Groovy

The Groovy language is bootstrapped on GNU Guix. Just like Clojure, Groovy
has a core written in Java that is just enough to build the parts of the
Groovy compiler. Unfortunately, Groovy relies on a Gradle build by default,
which in turns relies on Groovy as its build description language.

## Scala

We do not know about a bootsrap path for Scala. Going down the dependency chain
of Scala proved to be futile, as the first pulished version of Scala depends
on proprietary software. Most probably the only way to do this, after a
thoughtful mapping of problem space, seems to be writing a bootstrap compiler
for Scala. Some preliminary work has already been done in that direction, but
it is not ready for publication yet. People interested bootstrapping Scala
are welcome to inquire on the
[#bootstrappable IRC channel]("https://web.libera.chat/?randomnick=1&channels=%23bootstrappable&uio=d4")
on libera.chat.

## Kotlin

We do not know about a bootstrap path for Kotiln. We are in the phase of
mapping the dependencies of Kotlin to get a plan to have it bootstrapped.
People interested in bootstrapping Kotlin are welcome to join in.

[← back to list of projects](/projects.html)
