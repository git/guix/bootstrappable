title: Bootstapping Java Tools
---

## Maven

Maven is a build tool in the Java ecosystem. It needs Maven to build.
Maven is bootstrapped in GNU Guix, using generated build configuration
files for the Ant build tool, that is already bootstrapped as the result
of the [From C++ to the world of Java](/projects/java.html) project.

You can download the graph of dependencies from [here](/graphs/maven.dot)
in dot format.

## Gradle

Gradle is a build tool in the Java ecosystem. It needs Gradle to build.
It also depends on Groovy, Scala and Kotlin. Groovy is already bootstrapped,
but Scala is currently not bootstrappable and we do not know of a way to build
Kotlin either, to see the status of work in progress on the issue see
[Bootstrapping JVM Languages](/projects/jvm-languages.html).

[← back to list of projects](/projects.html)
