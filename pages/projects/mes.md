title: Working towards a source-based bootstrapping path to a GNU+Linux system]
---

### Reduced binary seed bootstrap

[Mes](https://www.gnu.org/software/mes) aims to create an entirely
source-based bootstrapping path.  The target is to have
[GNU Guix](https://guix.gnu.org) bootstrap from a minimal,
easily inspectable binary—that should be readable as source—into
something close to R6RS Scheme.

As bootstrapping is presumably easiest and probably most fun with
Scheme, Mes comes with mescc: a C compiler/linker to bootstrap into
GNU GCC and GNU Guile, via TinyCC.  Mes currently has an interpreter
written in C with garbage collector, a library of loadable Scheme
modules and test suite just barely enough to support a simple REPL and
a C compiler that can build TinyCC.  One of the next steps of this
project includes replacing the mes and mescc-tools binaries with M1
sources based on stage0.

The following the graph shows how in Guix GCC is removed from the
bootstrap binary seed.  This is the dependency graph of the bootstrap
as implemented in [GNU Guix](https://gnu.org/software/guix):

![dependency graph of the bootstrap in GNU Guix](/images/gcc-mesboot0.png)

- The nodes having elliptical shape are the static bootstrap binaries
  bundled into a single tarball; we show them to make implicit
  dependencies clear.

It is also possible to create a similar bootstrap path for other
distributions, as showcased by Nix at the Reproducible Build Summit.

### Build GCC 4.7 with a smaller compiler

GCC is a complex beast and a binary of it is often used to bootstrap
the whole system.  Version 4.7 is the last version of GCC to not
require a C++ compiler.  This project aims to build GCC version 4.7
with a simple C compiler such as
[TinyCC](https://www.landley.net/code/tinycc/).

[Since October
2019](https://guix.gnu.org/en/blog/2019/guix-reduces-bootstrap-seed-by-50/),
Guix bootstraps by using MesCC—the [small C
compiler](https://www.gnu.org/software/mes/manual/html_node/Invoking-mescc.html)
that comes with [Mes](https://www.gnu.org/software/mes)—to build TinyCC,
which is used to build GCC 2.95.0, which then builds GCC 4.7.4.

### Reducing the seed even further: stage0

[Stage0](https://savannah.nongnu.org/projects/stage0) starts with just
a 280 byte Hex monitor and builds up the infrastructure required to
start some serious software development.

With zero external dependencies, with the most painful work already
done and real languages such as assembly, Forth and garbage collected
Lisp already implemented.

Demonstrate some old school skill, by writing a new compiler or
interpreter or help with various support tasks required to keep this
project on track.  Or if low level programming isn't for you, help
bridge the gap between our goal and where we are by writing low
dependency software in the high level language of your choice that
might be easier for us to bootstrap.

Even if contributing to it isn't for you, if you could take 7 minutes
to verify what already exists on whatever weird operating system or
hardware you have; we would love to hear about your results.

[← back to list of projects](/projects.html)
