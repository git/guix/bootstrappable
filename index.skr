(post :title "Bootstrappable builds"
      (page
       (p [Do you know how to make yoghurt?  The first step is to add yoghurt to milk!  How can you build a compiler like ,(anchor "GCC" "https://gcc.gnu.org")?  The first step is to get a compiler that can compile the compiler.])

       (p [Compilers are often written in the language they are compiling.
           This creates a chicken-and-egg problem that leads users and distributors to rely on opaque, pre-built binaries of those compilers that they use to build newer versions of the compiler.])
       (p [To gain trust in our computing platforms, we need to be able to tell how each part was produced from source.
           We believe that opaque binaries are a threat to user security and user freedom since they are not auditable;
           our goal is to minimize the amount of bootstrap binaries.])

       (h2 [Benefits])
       (p [This is nice, but what are the ,(em [actual]) benefits of “bootstrappable” implementations?
           ,(anchor "Find out what additional benefits" "benefits.html") there are to achieving bootstrappable builds.])

       (h2 [Best practices])
       (p [Are you developing or contributing to software that is affected by the bootstrapping problem?
           Here we list ,(anchor "best practices and practical examples" "best-practices.html")
           that can help you pull yourself up by your own bootstraps.])

       (h2 [Collaboration projects])
       (p [Solving bootstrapping problems in existing compilers and build systems requires collaboration.
           Here is a ,(anchor "list of long-term high-impact projects" "projects.html")
           that we would like to work on collaboratively.])
       (p [More projects and status updates can be found ,(anchor "on the bootstrapping wiki" "https://bootstrapping.miraheze.org/").])
       (p [Join the ,(anchor "mailing list" "who.html") and/or the ,(anchor "IRC channel #bootstrappable" "https://web.libera.chat/?randomnick=1&channels=%23bootstrappable&uio=d4") on libera.chat for news and communication!])

       (h2 [Further reading])
       (ul (li [Ken Thompson's acceptance speech for the 1983 Turing Award:
                ,(anchor "Reflections on trusting trust"
                         "https://www.ece.cmu.edu/~ganger/712.fall02/papers/p761-thompson.pdf")])
           (li [,(anchor "Toy example of a subverted rust compiler"
                         "https://manishearth.github.io/blog/2016/12/02/reflections-on-rusting-trust/")])
           (li [,(anchor "What is a coder's worst nightmare?"
                         "https://www.quora.com/What-is-a-coders-worst-nightmare/answer/Mick-Stute")])
           (li [,(anchor "Defending Against Compiler-Based Backdoors"
                         "https://blog.regehr.org/archives/1241")])
           (li [,(anchor "Deniable Backdoors Using Compiler Bugs"
                         "https://www.alchemistowl.org/pocorgtfo/pocorgtfo08.pdf")]))))
