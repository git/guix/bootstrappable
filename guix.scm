;; -*- geiser-scheme-implementation: guile -*-
;;; Bootstrappable.org website
;;; Copyright © 2021 Ludovic Courtès <ludo@gnu.org>
;;;
;;; This file is part of the Bootstrappable.org website.
;;;
;;; The Bootstrappable.org website is free software; you can
;;; redistribute it and/or modify it under the terms of the Affero
;;; General Public License as published by the Free Software
;;; Foundation; either version 3 of the License, or (at your option)
;;; any later version.
;;;
;;; The Bootstrappable.org website is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
;;; Public License for more details.
;;;
;;; You should have received a copy of the Affero General Public
;;; License along with these source files.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;;
;;; Run 'guix build -f guix.scm' to build the web site.
;;;

(use-modules (gnu) (guix)
             ((guix git-download) #:select (git-predicate))
             (gnu packages base)
             (gnu packages guile-xyz))

(define this-directory
  (dirname (current-filename)))

(define source
  (local-file this-directory "bootstrappable.org-checkout"
              #:recursive? #t
              #:select? (git-predicate this-directory)))

(computed-file "bootstrappable.org"
               (with-imported-modules '((guix build utils))
                 #~(begin
                     (use-modules (guix build utils))

                     ;; Install a Unicode-capable locale.
                     (setenv "GUIX_LOCPATH"
                             #$(file-append glibc-utf8-locales
                                            "/lib/locale"))
                     (setenv "LC_ALL" "en_US.utf8")

                     (copy-recursively #$source ".")
                     (invoke #$(file-append haunt "/bin/haunt") "build")
                     (copy-recursively "site" #$output))))
